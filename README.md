# Teledetection Project

## Introduction

This code is a K-Mean variant used to detect clouds on teledetection images.

## Compilation

Use the provided Makefile to compile the "launch" program:

```
$ make launch
```

This software makes use of gtk 2.0, make sure it's installed before compiling.

## Usage

The launch program will find the images located on the executable directory and print the percentage of clouds detected for each one on the standard output.

```
$ ls
img1.png        img2.png        img3.png        launch          ...
$ ./launch
```
