.KEEP_STATE:

CC=gcc
CCFLAGS=-Wall -g3 -O0 `pkg-config --cflags gtk+-2.0` 
LDFLAGS=`pkg-config --libs gtk+-2.0`

OBJ_FILES=compute.o main.o

all: launch

.c.o:
	${CC} ${CCFLAGS} -o $@ -c $<

launch:${OBJ_FILES}
	${CC} -o $@ ${OBJ_FILES} ${LDFLAGS}

clean:
	rm -f *.o launch

install:
	cp *.h ${INC}

.SUFFIXES: .o .c .h
