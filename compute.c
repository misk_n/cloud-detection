#include <stdio.h>
#include <gtk/gtk.h>
#include <stdlib.h>

#include <limits.h>

struct vector
{
  unsigned coefficients[5];
};

struct pixel
{
  guchar components[3];
};

static inline void swap(unsigned *a, unsigned *b)
{
  guchar tmp = *a;
  *a = *b;
  *b = tmp;
}

static void grayscale(struct pixel *dst,
                      unsigned width,
                      unsigned height,
                      const struct pixel* src)
{
  for (unsigned i = 0; i < height; i++)
  {
    for (unsigned j = 0; j < width; j++)
    {
      unsigned pos = i * width + j;
      unsigned mean = 0;
      for (unsigned k = 0; k < 3; k++)
        mean += src[pos].components[k];
      mean /= 3;
      for (unsigned k = 0; k < 3; k++)
        dst[pos].components[k] = mean;
    }
  }
}

static void print_results(struct pixel *dst,
                          unsigned width,
                          unsigned height,
                          unsigned *pixel_class,
                          unsigned nb_classes,
                          unsigned iterations)
{
  unsigned count = 0;
  for (unsigned i = 0; i < height; i++)
  {
    for (unsigned j = 0; j < width; j++)
    {
      unsigned pos = i * width + j;
      if (pixel_class[pos] == nb_classes - 1)
      {
        count++;
        dst[pos].components[0] = 0;
        dst[pos].components[1] = 255;
        dst[pos].components[2] = 0;
      }
    }
  }
  float percents = ((float)count * 100) / ((float)(height * width));
  printf("%f%%\n", percents);
}

static void init_vectors(struct vector *pixel_positions,
                         unsigned width, unsigned height,
                         struct pixel *gray_image)
{
  for (unsigned i = 0; i < height; i++)
  {
    for (unsigned j = 0; j < width; j++)
    {
      unsigned pos = i * width + j;
      pixel_positions[pos].coefficients[0] = gray_image[pos].components[0];
      pixel_positions[pos].coefficients[1] = i == 0 ?
        0 : gray_image[pos - width].components[0];
      pixel_positions[pos].coefficients[2] = i == height - 1 ?
        0 : gray_image[pos + width].components[0];
      pixel_positions[pos].coefficients[3] = j == 0 ?
        0 : gray_image[pos - 1].components[0];
      pixel_positions[pos].coefficients[4] = j == width - 1 ?
        0 : gray_image[pos + 1].components[0];
      // sort coefficients
      int changed = 0;
      while(changed != -1)
      {
        int tmp = changed;
        changed = -1;
        for (int k = tmp; k < 4; k++)
        {
          if (pixel_positions[pos].coefficients[k] < pixel_positions[pos].coefficients[k + 1])
          {
            swap(pixel_positions[pos].coefficients + k, pixel_positions[pos].coefficients + k + 1);
            changed = changed < pos ? changed : pos;
          }
        }
      }
    }
  }
}

static void init_class_positions(struct vector *class_positions,
                                 unsigned nb_classes,
                                 unsigned maxValue)
{
  for (unsigned i = 0; i < nb_classes; i++)
  {
    for (unsigned c = 0; c < 5; c++)
    {
      class_positions[i].coefficients[c] = (maxValue / nb_classes) * (i + 1);
    }
  }
}

static unsigned euclidian_distance(const struct vector *a, const struct vector *b)
{
  unsigned res = 0;
  int tmp = 0;
  for (unsigned i = 0; i < 5; i++)
  {
    tmp = a->coefficients[i] - b->coefficients[i];
    tmp *= tmp;
    res += tmp;
  }
  return res;
}

static unsigned compute_classes(const struct vector *pixel_positions,
                                const struct vector *class_positions,
                                unsigned *pixel_class,
                                unsigned width, unsigned height,
                                unsigned nb_classes)
{
  unsigned changes = 0;
  for (unsigned i = 0; i < height; i++)
  {
    for (unsigned j = 0; j < width; j++)
    {
      unsigned pos = i * width + j;
      unsigned distance = UINT_MAX;
      unsigned class = 0;
      for (unsigned c = 0; c < nb_classes; c++)
      {
        unsigned d = euclidian_distance(class_positions + c, pixel_positions + pos);
        if (d < distance)
        {
          distance = d;
          class = c;
        }
      }
      if (class != pixel_class[pos])
      {
        changes++;
        pixel_class[pos] = class;
      }
    }
  }
  return changes;
}

void ComputeImage(guchar *pucImaOrig, 
		  int NbLine,
		  int NbCol, 
		  guchar *pucImaRes)
{
  const void *src = pucImaOrig;
  void *dst = pucImaRes;
  grayscale(dst, NbCol, NbLine, src);

  const unsigned nb_classes = 8;
  const unsigned maxValue = 230;
  const unsigned maxChanges = 0;

  struct vector *pixel_positions = malloc(NbCol * NbLine * sizeof (struct vector));
  struct vector *class_positions = malloc(nb_classes * sizeof (struct vector));
  unsigned *pixel_class = calloc(NbCol * NbLine, sizeof (unsigned));
  unsigned nb_px_per_class[nb_classes];

  init_vectors(pixel_positions, NbCol, NbLine, dst);
  init_class_positions(class_positions, nb_classes, maxValue);

  unsigned iterations = 0;
  for (unsigned changes = maxChanges + 1; changes > maxChanges;)
  {
    changes = 0;
    iterations++;
    changes = compute_classes(pixel_positions, class_positions,
                              pixel_class,
                              NbCol, NbLine, nb_classes);

    for (unsigned i = 0; i < nb_classes; i++)
    {
      nb_px_per_class[i] = 0;
      for(unsigned j = 0; j < 5; j++)
        class_positions[i].coefficients[j] = 0;
    }
    for (unsigned i = 0; i < NbCol * NbLine; i++)
    {
      unsigned class = pixel_class[i];
      for (unsigned j = 0; j < 5; j++)
        class_positions[class].coefficients[j] += pixel_positions[i].coefficients[j];
      nb_px_per_class[class]++;
    }
    for (unsigned i = 0; i < nb_classes; i++)
      for (unsigned j = 0; j < 5; j++)
        if (nb_px_per_class[i])
          class_positions[i].coefficients[j] /= nb_px_per_class[i];
  }

  print_results(dst, NbCol, NbLine, pixel_class, nb_classes, iterations);

  free(pixel_positions);
  free(class_positions);
  free(pixel_class);
}
