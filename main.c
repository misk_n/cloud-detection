#include <gtk/gtk.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>

#include "compute.h"

int main(int argc, char *argv[])
{
  DIR* rep = NULL;
  char buf[255];
  readlink("/proc/self/exe", buf, 255);
  rep = opendir(dirname(buf));
  if (rep == NULL)
  {
    printf("Unable to opendir\n");
    exit(1);
  }

  struct dirent* dent;
  while ((dent = readdir(rep)) != NULL)
  {
    if (!strcmp(dent->d_name, ".") || !strcmp(dent->d_name, ".."))
      continue;
    unsigned len = strlen(dent->d_name) + strlen(buf) + 2;
    char *file_path = calloc(sizeof (char), len);
    strcat(file_path, buf);
    strcat(file_path, "/");
    strcat(file_path, dent->d_name);

    GdkPixbuf* pGdkPixbufImaIn = gdk_pixbuf_new_from_file (file_path, NULL);

    if (pGdkPixbufImaIn == NULL){
      free(file_path);
      continue;
    }

    printf("%s : ", dent->d_name);

    int NbCol=gdk_pixbuf_get_width(pGdkPixbufImaIn);
    int NbLine=gdk_pixbuf_get_height(pGdkPixbufImaIn);
    guchar *pucImaOrig=gdk_pixbuf_get_pixels(pGdkPixbufImaIn);
    GdkPixbuf* pGdkPixbufImaRes=gdk_pixbuf_copy(pGdkPixbufImaIn);
    guchar *pucImaRes=gdk_pixbuf_get_pixels(pGdkPixbufImaRes);
    ComputeImage(pucImaOrig, NbLine, NbCol, pucImaRes);

    free(file_path);
  }

  if (closedir(rep) != 0)
  {
    printf("Could not close directory\n");
    return 2;
  }
}
